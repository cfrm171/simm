# simm

Initial Margin (IM) is the amount of collateral required to open a position with a broker or an exchange or a bank. The Standard Initial Margin Model (SIMM) is very likely to become the market standard. It is designed to provide a common methodology 